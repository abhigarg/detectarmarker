# -*- coding: utf-8 -*-
"""
Created on Fri Dec 12 19:43:29 2014

@author: abgg
"""

import cv2
import numpy as np
import cv2.cv  as cv
import math


########### check if the contours are from marker or not ###############

def isMarker(apcnt, eps):
    cx = 0
    cy = 0
    for i in range(0,4):
        cnt = tuple(approx[i][0])
        cx = cx + cnt[0]
        cy = cy + cnt[1]
    cx = cx/4
    cy = cy/4
    print "cx: "
    print cx
    print "cy: "
    print cy

    dd1 = ((cx-tuple(approx[0][0])[0])**2 + (cy-tuple(approx[0][0])[1])**2)**(0.5)
    dd2 = ((cx-tuple(approx[1][0])[0])**2 + (cy-tuple(approx[1][0])[1])**2)**(0.5)
    dd3 = ((cx-tuple(approx[2][0])[0])**2 + (cy-tuple(approx[2][0])[1])**2)**(0.5)
    dd4 = ((cx-tuple(approx[3][0])[0])**2 + (cy-tuple(approx[3][0])[1])**2)**(0.5)

    print "differences: "
    print "dd1 - dd2: "
    print abs(dd1-dd2)
    print "dd1 - dd3: "
    print abs(dd1-dd3)
    print "dd1 - dd4: "
    print abs(dd1-dd4)

    return abs(dd1-dd2) < eps and abs(dd1-dd3) < eps and abs(dd1-dd4) < eps


#### calculate code from marker image ####


def calCode(patt_img,sz):
    
    height, width = patt_img.shape

    print "marker height: "
    print height

    print "marker width: "
    print width

    gap_h = 42
    gap_w = 42

    ht = 340
    wt = 340
    
    gh = math.ceil(gap_h*height/ht)
    gw = math.ceil(gap_w*width/wt)

    print "gap height: "
    print gh

    print "gap width: "
    print gw

    ht2 = height - gh
    wt2 = width - gw

    patt = patt_img[gw:wt2, gh:ht2]
    
    test = cv2.cvtColor(patt, cv2.COLOR_GRAY2BGR)

    h = int((ht2-gh)/sz)
    w = int((wt2-gw)/sz)

    ht2 = int(ht2)
    wt2 = int(wt2)
    
    print "h:"
    print h
    print "w:"
    print w
    code = 0
    mode_val = 0
    i = 0
    j = 0
    countNZ = 0
    counter = 0
    count_row = 0

    while(i < (sz-1)*h+1):
        print "i:"
        print i
        while(j < (sz-1)*w+1):
            print "j:"
            print j
            cv2.rectangle(test, (i,j),(i+h,j+w), (255, 255, 0), 2)
            cv2.imshow('test',test)
            cv2.waitKey(0)
            countNZ = cv2.countNonZero(patt[j:j+w,i:i+h])
            j = j+w
            
            if(countNZ > h*w - countNZ):
                print "countNZ:"
                print countNZ
                
                print "value:"
                print pow(2,counter)
                code = code + pow(2, counter)
                countNZ = 0;
            counter = counter + 1

        j = 0
        i = i + h

    cv2.destroyAllWindows()

    return code


##### main function ######

sz = 3

fname = '3.jpg'

splitfname = fname.split('.',1)

img = cv2.imread(fname,0)

cimg = cv2.imread(fname,1)

# adaptive threshold
thresh = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,251,0)


# find contours
(cnts, _) = cv2.findContours(thresh.copy(),cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)


target_corners = []
target_contours = []

nTargets = 0

# loop over our contours
for c in cnts:
    cnt_area = cv2.contourArea(c)
    cnt_loop = 0
    if (cnt_area > 200 and cnt_area < 10000):    
        # approximation
        epsilon = 0.02*cv2.arcLength(c,True)
        approx = cv2.approxPolyDP(c,epsilon,True)
        if (len(approx) == 4 and isMarker(approx, 5)):
            cv2.drawContours(cimg, approx, -1, (0, 255, 0), 2)
            pts = np.array((4,2), dtype = 'float32')
            pts = np.vstack(approx).squeeze()
            
            srcPts = np.zeros((4, 2), dtype = "float32")
            s = pts.sum(axis = 1)
            srcPts[0] = pts[np.argmin(s)]
            srcPts[2] = pts[np.argmax(s)]
            
            diff = np.diff(pts, axis = 1)
            srcPts[1] = pts[np.argmin(diff)]
            srcPts[3] = pts[np.argmax(diff)]
            
            (tl, tr, br, bl) = srcPts

            tc = np.zeros((1,2), dtype = "float32")
            
            widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
            widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
            maxWidth = max(int(widthA), int(widthB))
            maxWidth = int(2*maxWidth)
            print "new test marker"
            print "maxwidth: "
            print maxWidth
            
            heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
            heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
            maxHeight = max(int(heightA), int(heightB))
            maxHeight = int(2*maxHeight)
            print "maxheight"
            print maxHeight

            # check if marker has similar height and width
            if(abs(maxWidth - maxHeight) < 0.3*max(maxWidth, maxHeight)):
                
                dstPts = np.array([[0, 0],[maxWidth - 1, 0],[maxWidth - 1, maxHeight - 1],[0, maxHeight - 1]], dtype = "float32")
                
                tc[0][0] = (tl[0]+tr[0]+br[0]+bl[0])/4
                tc[0][1] = (tl[1]+tr[1]+br[1]+bl[1])/4                
                print "target center"
                print tc
                nTargets = nTargets + 1
                target_center.append(tc)
                
                ntc = len(target_center[0])
                print "ntc"
                print ntc
                if target_center:
                    for i in range(0,ntc-1):
                        tci = target_center[i]
                        print tci
                        dist = np.sqrt((tc[0][0]-tci[0][0])**2 + (tc[0][1]-tci[0][1])**2)
                        print "dist:"
                        print dist
                        if(dist < 10):
                            cnt_loop = 1
                            break                        
                        
                if (cnt_loop == 1):                
                    continue
                            
                for i in range(0,4):
                    cnt = tuple(approx[i][0])
                    cv2.circle(cimg, cnt, 3, (255, 0, 0), 1)
                    
                # perform perspective transform
                M = cv2.getPerspectiveTransform(srcPts, dstPts)

                # wraped image
                srcPatt = img
                warpedPatt = cv2.warpPerspective(srcPatt, M, (maxWidth, maxHeight))
                col, row = warpedPatt.shape
                print "col:"
                print col

                print "row:"
                print row

                cv2.imshow('p1', srcPatt)
                cv2.imshow('warped', warpedPatt)
                k = cv2.waitKey(0)
                if k == 'q':
                    break;
                
                cv2.destroyAllWindows()

                # calculate code from wraped image
                thresh_warped = cv2.adaptiveThreshold(warpedPatt,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,51,0)
                
                code = 0
                code = calCode(thresh_warped, sz)
                threshfname = 'thresh_' + str(code) + '_' + splitfname[0] + '.jpg'
                cv2.imwrite(threshfname, warpedPatt)
                print "code: "
                print code
                cv2.putText(cimg,str(code), (int(tl[0]),int(tl[1])-10), cv2.FONT_HERSHEY_PLAIN, 0.7, (255, 0 ,0))


cv2.imshow('contours', cimg)
cv2.waitKey(0)
cv2.destroyAllWindows()

svfname = 'output_' + splitfname[0] + '.jpg'
cv2.imwrite(svfname, cimg)




